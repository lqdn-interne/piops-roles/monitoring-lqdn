# Surveillance et monitoring LQDN

![Logo](https://git.laquadrature.net/uploads/-/system/project/avatar/320/293344.png)

Ce rôle n'est pas utilisé en tant que tel, mais sert de référence. Nous utilisons les rôles suivants ; 

- https://github.com/cloudalchemy/ansible-grafana
- https://github.com/cloudalchemy/ansible-node-exporter
- https://github.com/cloudalchemy/ansible-prometheus


